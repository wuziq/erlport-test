defmodule App.App do

  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      worker(App.Server, []),
    ]

    options = [
      name: App.Supervisor,
      strategy: :simple_one_for_one,
    ]

    Supervisor.start_link(children, options)
    |> IO.inspect
  end

end
