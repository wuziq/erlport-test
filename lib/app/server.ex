defmodule App.Server do

  alias App.PythonHelper

  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, nil)  # nil cuz no args
    |> IO.inspect
  end

  # returns initial state that will henceforth be managed by this server
  def init(_) do  # there's got to be a way to pass in python scripts path and which python via starting elixir runtime
    [File.cwd!, "python"]
    |> Path.join()
    |> to_charlist()
    |> PythonHelper.start_instance('python3')
    |> IO.inspect
  end

  def handle_call({:poke}, _from, python_pid) do
    {:reply, {python_pid, :some_poked_state}, :some_poked_state}
  end

  def handle_call({:call_function, module, function, args}, _from, python_pid) do
    result = PythonHelper.call_instance(python_pid, module, function, args)
    {:reply, result, python_pid}
  end

end
