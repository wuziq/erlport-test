defmodule ErlportTest do
  def new do
    {:ok, pid} = Supervisor.start_child(App.Supervisor, [])
    pid
  end

  def poke(pid) do
    GenServer.call(pid, {:poke})
  end

  def py_hello(pid) do
    GenServer.call(pid, {:call_function, :mypython, :hello, []})
  end
end
