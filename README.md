# Output
```
wuziqs-MacBook-Pro:erlport_test wuziq$ iex -S mix
Erlang/OTP 23 [erts-11.1.8] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe] [dtrace]

warning: :simple_one_for_one strategy is deprecated, please use DynamicSupervisor instead
  (elixir 1.11.4) lib/supervisor.ex:604: Supervisor.init/2
  (elixir 1.11.4) lib/supervisor.ex:556: Supervisor.start_link/2
  (erlport_test 0.1.0) lib/app/app.ex:17: App.App.start/2
  (kernel 7.2.1) application_master.erl:277: :application_master.start_it_old/4

{:ok, #PID<0.189.0>}
Interactive Elixir (1.11.4) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> pid = ErlportTest.new
warning: Supervisor.start_child/2 with a list of args is deprecated, please use DynamicSupervisor instead
  (elixir 1.11.4) lib/supervisor.ex:826: Supervisor.start_child/2
  (erlport_test 0.1.0) lib/erlport_test.ex:3: ErlportTest.new/0
  (stdlib 3.14) erl_eval.erl:680: :erl_eval.do_apply/6
  (stdlib 3.14) erl_eval.erl:449: :erl_eval.expr/5
  (elixir 1.11.4) src/elixir.erl:280: :elixir.recur_eval/3
  (elixir 1.11.4) src/elixir.erl:265: :elixir.eval_forms/3

{:ok, #PID<0.193.0>}
{:ok, #PID<0.192.0>}
#PID<0.192.0>
iex(2)> ErlportTest.p
poke/1        py_hello/1
iex(2)> ErlportTest.py_hello(pid)
'hi from python'
iex(3)>
```
